import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, LoadingController, Loading } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { User } from '../../providers/user';
import { Api } from '../../providers/api';
import { TranslateService } from '@ngx-translate/core';
import { MainPage } from '../../pages/pages';
import base64 from 'base-64';
/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {
account: { login: string, password: string } = {
    login: 'traffic_angel',
    password: 'Password1'
  };loading: Loading;

  // Our translated text strings
  private loginErrorString: string;
  constructor(
  	public navCtrl: NavController,
  	public user: User,
    public api: Api,
  	public toastCtrl: ToastController,
    public translateService: TranslateService, private loadingCtrl: LoadingController
  	) { 
  	this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  	}

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }
  login() {
    this.navCtrl.push(LoginPage);
  }

  signup() {
    this.navCtrl.push(SignupPage);
  }

  	// Attempt to login in through our User service
  	doLogin() {      
      this.account.password = this.account.password ? base64.encode(this.account.password) : null;
      this.showLoading()
      this.api.post('users/login', this.account).subscribe((resp) => {
      this.loading.dismiss();
      var response = JSON.parse(resp["_body"]);
        console.log(response);
        if (response.code === 200){
          this.loginErrorString = "Welcome back! @"+response.data.current_user.username
          this.api.setUserSession(response.data.authentication_token, response.data.current_user);
          this.navCtrl.setRoot("FeedsPage");  
        } else {
          this.loginErrorString = response.errors.join(', ')
          this.account.password = "";
        }
        this.show_toast(this.loginErrorString);      
    }, (err) => {
      this.loading.dismiss();
      this.account.password = "";
      // this.navCtrl.push(MainPage);
      // Unable to log in
        let error = JSON.parse(err["_body"]);
        this.show_toast(this.loginErrorString);
    });
  }

  show_toast(message){
          let toast = this.toastCtrl.create({
        message: message,
        duration: 8000,
        position: 'bottom',
        showCloseButton: true
      });
      toast.present();  
  }  
}
