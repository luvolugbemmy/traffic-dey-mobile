import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api';
/**
 * Generated class for the CommentsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {
	traffic_report;
  constructor(public navCtrl: NavController, public navParams: NavParams, private api: Api) {
  	this.traffic_report = navParams.get("traffic_report");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentsPage');
  }

}
