import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { IonicPage, Platform, NavController, ModalController, ToastController, AlertController, LoadingController, Loading, ActionSheetController, Content } from 'ionic-angular';
import { Api } from '../../providers/api'; 
// import { AutoCompleteApi } from '../../providers/auto-complete-api'; 
import { Geolocation } from '@ionic-native/geolocation';
// map: GoogleMap;, private googleMaps: GoogleMaps import { GoogleMaps, GoogleMap, GoogleMapsEvent, LatLng, CameraPosition, MarkerOptions, Marker} from '@ionic-native/google-maps';
import { CompleterService, CompleterData } from 'ng2-completer';
// import google from 'google-maps';
// import { DbltapDirective } from '../../directives/dbltap/dbltap';
declare var google;
declare var navigator;

@IonicPage()
@Component({
  selector: 'page-feeds',
  templateUrl: 'feeds.html',
})
export class FeedsPage {
  traffic_reports;loading: Loading;report_limit_start;report_limit_size;report_count;loadMoreData;map;
  keyword;coords;comment;view_type;element: HTMLElement;user_settings;geolocation_options;followers;segments;
  show_followers;items: string[] = ["Noah", "Liam", "Mason", "Jacob"];hElement;
  activeClassName;likedClassName;demoImageContainer;theHeartImage;
   @ViewChild('map') mapElement: ElementRef;
   @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController, private api: Api, public geolocation: Geolocation, 
    public modalCtrl: ModalController, private platform: Platform, public renderer: Renderer,
    private toastCtrl: ToastController, private alertCtrl: AlertController, public actionSheetCtrl: ActionSheetController,
    private loadingCtrl: LoadingController) {
    this.traffic_reports = [];
    this.report_limit_start = 0;
    this.report_limit_size = 20;
    this.report_count = -1;
    this.loadMoreData = true;
    this.show_followers = false;
    this.keyword = "";
    this.coords = {};
    this.comment = {};
    this.followers = [];
    this.map = null;
    this.segments = "route";
    this.user_settings = this.api.get_users_settings();  
    this.view_type = this.user_settings.feeds_view;
    this.activeClassName = 'active';
    this.likedClassName = 'liked';              
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    let infiniteScroll = {};
    this.hElement = this.content._elementRef.nativeElement;        
    this.set_geo_location_and_fetch_reports();
    // this.get_followers(); 
    // setTimeout(() => {
    //   this.set_view_type(this.view_type); 
    // }, 3000);
  }

  onSegmentChange(){
    if(this.segments === "route"){
      this.traffic_reports = [];
      this.report_limit_start = 0;
      this.report_limit_size = 20;
      this.report_count = -1;
      this.loadMoreData = true;      
      this.set_geo_location_and_fetch_reports();
    }else if(this.segments === "pinned"){
      this.traffic_reports = [];
      this.report_limit_start = 0;
      this.report_limit_size = 20;
      this.report_count = -1;
      this.loadMoreData = true;       
      this.fetch_pinned_location_reports();      
    }        
  }

  // format_reports(){
  //   for (let traffic_report of this.traffic_reports){
  //     if (traffic_report.i_liked){
  //       traffic_report.cardClassName = "card-container liked";
  //     }else{
  //       traffic_report.cardClassName = "card-container";
  //     }
  //   }
  // }

  set_view_type(vtype){
    // if (this.view_type != vtype) {      
      this.showLoading();
      setTimeout(() => {
        this.view_type = vtype;
        if(vtype === 'map'){
          this.element = this.mapElement.nativeElement;
          // this.map = this.googleMaps.create(this.element)
          this.loadMap();
        }
        this.loading.dismiss();
      }, 2000);
    // }
  }

  doRefresh(refresher) {
      // console.log('Begin async operation', refresher);
      this.onSegmentChange();
      setTimeout(() => {
        console.log('Async operation has ended');
        refresher.complete();
      }, 1000);
    }   
  
  fetch_pinned_location_reports(){
    let params = [];
    params.push("limit_start="+this.report_limit_start);
    params.push("limit_size="+this.report_limit_size);
    this.showLoading();
    this.api.get("traffic_reports/saved_locations_reports?"+params.join("&"))
    .subscribe((resp) => {
      this.loading.dismiss();
      let response = JSON.parse(resp["_body"]);
        console.log(response);
        if (response.code === 200 && response.data.count > 0){
          this.report_count = response.data.count;
          this.traffic_reports = this.traffic_reports.concat(response.data.traffic_reports);
          this.report_limit_start = this.report_limit_start+this.report_limit_size;
          if(this.traffic_reports.length === this.report_count){
             this.loadMoreData = false;
          }  
         // this.set_view_type(this.view_type);         
        }  else {
          let e = response.errors || []
          this.showToast(response.code+": No traffic reports found around your saved locations "+e.join(', '), 6000, "bottom");
        }             
    }, (err) => {
      this.loading.dismiss();
      this.showToast("Error while fetching traffic reports", 4000, "bottom");      
    });    
  }
  
  get_my_current_location(){
    this.geolocation.getCurrentPosition(this.user_settings.geolocation).then((resp) => {
      this.coords = resp.coords;
    },
    (error) => {
      // this.loading.dismiss();
      console.log("Error using Native geolocator", JSON.stringify(error))
      this.use_browser_geolocation();      
      // this.showToast("Couldn't get your location using your phone geolocation. Switching over to browser navigator.", 6000, "bottom");      
    }); 
  }

  set_geo_location_and_fetch_reports(){
    this.showLoading();
    this.geolocation.getCurrentPosition(this.user_settings.geolocation).then((resp) => {
      this.coords = resp.coords;
      this.fetch_traffic_reports();
      this.loading.dismiss();
    },
    (error) => {
      this.loading.dismiss();
      console.log("Error using Native geolocator", JSON.stringify(error))
      this.use_browser_geolocation();      
      // this.showToast("Couldn't get your location using your phone geolocation. Switching over to browser navigator.", 6000, "bottom");      
    });  
  }


  use_browser_geolocation(){
    let caller = this;
    this.showLoading();
    navigator.geolocation.getCurrentPosition(function(position){
        caller.coords = position.coords;
        caller.fetch_traffic_reports();
        caller.loading.dismiss();
      },
      function(error){
        caller.loading.dismiss();
        console.log("Error using browser geolocator", JSON.stringify(error))
        caller.showToast("Couldn't get your device location. Please check that your device location is turned on.", 6000, "bottom");      
      },
      caller.user_settings.geolocation
    );
  }
  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create("FeedCreatePage");
    addModal.onDidDismiss(item => {
      if (item) {
        // this.items.add(item);
        this.traffic_reports.unshift(item);
      }
      // this.fetch_traffic_reports();
    })
    addModal.present();
  }

  like_traffic_report(traffic_report, index){        
    this.api.get("traffic_reports/toggle_like/"+traffic_report.id)
    .subscribe((resp) => {
      this.loading.dismiss();
      let response = JSON.parse(resp["_body"]);
        console.log(response);
        if (response.code === 200){
          this.traffic_reports[index] = response.data.traffic_report;
          if(response.data.traffic_report.i_liked){
          this.addClass(this.demoImageContainer, this.activeClassName, index)
          this.demoImageContainer = this.hElement.querySelector('#js-card-container'+'-'+traffic_report.id);          
          this.theHeartImage = this.hElement.querySelector('#js-heart-image'+'-'+traffic_report.id);
            setTimeout(() => {
               this.removeClass(this.demoImageContainer, this.activeClassName, index)
            }, 800);
          }                        
        }             
    }, (err) => {      
      this.loading.dismiss();
    });    
  }

  addClass(domObj, className, index) {
    // if(domObj) {
      this.traffic_reports[index].card_class += ' ' + className;  
    // }  
  }

   removeClass(domObj, className, index) {
    // if(traffic_report.cardClassName)
    this.traffic_reports[index].card_class = this.traffic_reports[index].card_class.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');    
    console.log("sdsdsdsd")
  }


  filter_followers(event){
    console.log(event)
    let array = this.comment.comment.split("@");
    let keyword = array[array.length-1];
    console.log(keyword);
    if(keyword){
      this.show_followers = true;
      return this.followers.filter(item => item.username.toLowerCase().startsWith(keyword.toLowerCase()));
    } else {
      this.show_followers = false;
      return []
    }    
  }

  // get_followers() {
  //   this.api.get("users/my_followers/")
  //   .subscribe((resp) => {
  //     let response = JSON.parse(resp["_body"]);
  //       console.log(response);
  //       if (response.code === 200){
  //         this.followers = response.data.followers;          
  //       }             
  //   }, (err) => {      
  //     console.log(err);
  //   });    
  // }

  toggle_comment_box(traffic_report, index){
    if (traffic_report.show_comment_field){
      traffic_report.show_comment_field = false;
    } else {
      traffic_report.show_comment_field = true;
    }
  }

   presentActionSheet(traffic_report, index) {
     let actionSheet = this.actionSheetCtrl.create({
       // title: 'User Settings',
       buttons: [         
         {
           text: 'Share',
           handler: () => {
            console.log('Share to facebook');            
           }
         }         
       ]
     });

     if(traffic_report.user_id == this.api.getUserSession().id){
       actionSheet.addButton(
        {
           text: 'Delete',
           role: 'destructive',
           handler: () => {
             this.delete_traffic_report(traffic_report, index)
           }
         }
       )
     }

     actionSheet.addButton(
      {
         text: 'Cancel',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
    )     

     actionSheet.present();
   }  

  get_last_two_comments(traffic_report, index){
    if(traffic_report.comments.length > 2 ){
      return traffic_report.comments.slice(Math.max(traffic_report.comments.length - 2, 1));
    } else {
      return traffic_report.comments;
    }
  }

  load_more_comments(traffic_report, index){
    this.navCtrl.push("CommentsPage", {traffic_report: traffic_report})
  }

  postComment(traffic_report, index){
    console.log(this.comment);
    this.api.post("traffic_reports/add_comment/"+traffic_report.id, {comment: this.comment[traffic_report.id]})
    .subscribe((resp) => {      
      let response = JSON.parse(resp["_body"]);
        console.log(response);
        if (response.code === 200){
           this.traffic_reports[index] = response.data.traffic_report;
           this.comment[traffic_report.id] = "";   
        }  else {
          let e = response.errors || []
          this.showToast(response.code+": Comment not added. "+e.join(', '), 3000, "bottom");
        }             
    }, (err) => {          
      this.showToast("Error: Comment not added", 3000, "bottom");
    }); 
  }


  fetch_traffic_reports(){
    let params = [];
    params.push("limit_start="+this.report_limit_start);
    params.push("limit_size="+this.report_limit_size);
    params.push("latitude="+this.coords.latitude);
    params.push("longitude="+this.coords.longitude);
    params.push("distance="+this.user_settings.distance);
    this.api.get("traffic_reports?"+params.join("&"))
    .subscribe((resp) => {
      // this.loading.dismiss();
      let response = JSON.parse(resp["_body"]);
        console.log(response);
        if (response.code === 200 && response.data.count > 0){
          this.report_count = response.data.count;
          this.traffic_reports = this.traffic_reports.concat(response.data.traffic_reports);
          this.report_limit_start = this.report_limit_start+this.report_limit_size;
          if(this.traffic_reports.length === this.report_count){
             this.loadMoreData = false;
          }  
         // this.set_view_type(this.view_type);         
        }  else {
          let e = response.errors || [];
          this.showToast(response.code+": No Traffic reports fetched. You might want to increase search distance in your settings. "+e.join(', '), 6000, "bottom");
        }             
    }, (err) => {
      // this.loading.dismiss();
      this.showToast("Error while fetching traffic reports", 4000, "bottom");      
    });
  }


  delete_traffic_report(traffic_report, index){
    let confirm = this.alertCtrl.create({
      title: "Delete Traffic Report",
      message: "Are you sure?",
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Ok',
          handler: () => { 
            this.showLoading();    
            this.api.delete("traffic_reports/"+traffic_report.id)
            .subscribe((resp) => {
              this.loading.dismiss();
              let response = JSON.parse(resp["_body"]);
                console.log(response);
                if (response.code === 200){
                   this.traffic_reports.splice(index, 1); 
                }  else {
                  let e = response.errors || []
                  this.showToast(response.code+": Traffic report could not be deleted "+e.join(', '), 3000, "bottom");
                }             
            }, (err) => {     
              this.loading.dismiss(); 
              this.showToast("Error: Traffic report could not be deleted ", 3000, "bottom");
            });
           }
          }
        ]
      });
      confirm.present();                     
  }

  get_reverse_geocode(){
    this.showLoading();
    this.geolocation.getCurrentPosition(this.user_settings.geolocation).then((resp) => {
      this.coords = resp.coords;
    this.api.post("users/reverse_geocode_user_coords", {latitude: this.coords.latitude, longitude: this.coords.longitude})
      .subscribe((resp) => {
        this.loading.dismiss();
        let response = JSON.parse(resp["_body"]);
          console.log(response);
          if (response.code === 200 && response.suggested_locations.length > 0){            
            this.showCurrentLocations(response.suggested_locations)
          } else {
            this.showToast("No locations found.", 5000, "bottom")
          }  
      }, (err) => {
        this.loading.dismiss();
        this.showToast("Reverse geocode API Error", 5000, "bottom")
        console.log("Reverse geocode API", err)
      });
    },
    (error) => {
      this.loading.dismiss();
      console.log("Error using Native geolocator", JSON.stringify(error))
      // this.use_browser_geolocation();      
    });
  }

  showCurrentLocations(suggested_locations){
    let body = {name: null, latitude: null, longitude: null}
    let alert = this.alertCtrl.create({cssClass: "full-alert-box"});
    let title = "Locations";
    alert.setTitle(title);
    let klass = this;
    suggested_locations.forEach(function(location, index, arr){
          alert.addInput({
          type: 'radio',
          label: location.full_address,
          value: location,
          checked: false,
      });
    });
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        if(data){
          body.name = data.full_address;
          body.latitude = ""+data.lat;
          body.longitude = ""+data.lng;
          this.saveLocation(body) 
        }
      }
    });
    alert.present();
  }

  saveLocation(body){
    this.showLoading();
    this.api.post("users/save_current_location/", body)
    .subscribe((resp) => {      
      let response = JSON.parse(resp["_body"]);
        console.log(response);
        this.loading.dismiss();
        if (response.code === 200){
         this.showToast("Success: Location pinned!", 3000, "bottom");
        }  else {
          let e = response.errors || []
          this.showToast(response.code+": Current location not saved"+e.join(', '), 3000, "bottom");
        }             
    }, (err) => {
      this.loading.dismiss();          
      this.showToast("Error: Current location not saved", 3000, "bottom");
    });    
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }
 

  showToast(message, duration, position) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position,
      showCloseButton: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }  
  
loadMap() {
  console.log(this.element);
    // make sure to create following structure in your view.html file
    // and add a height (for example 100%) to it, else the map won't be visible
    // <ion-content>
    //  <div #map id="map" style="height:100%;"></div>
    // </ion-content>

    // create a new map by passing HTMLElement
    // let element: HTMLElement = document.getElementById('map');

    // let map: GoogleMap = this.googleMaps.create(element);

      if (this.platform.is('cordova') === true) {
        this.initJSMaps(this.element); //this.initNativeMaps(this.element); // SAMUEL OLUGBEMI: To change this later... let's use Google JS SDK for now 
      } else {
        this.initJSMaps(this.element);
      }

   



    //  const marker: Marker = map.addMarker(markerOptions)
    //    .then((marker: Marker) => {
    //       marker.showInfoWindow();
    //     });
    //  }

    // initJSMaps(mapEle) {
    //   new google.maps.Map(mapEle, {
    //     center: { lat: 43.071584, lng: -89.380120 },
    //     zoom: 16
    //   });
    // }

    // initNativeMaps(mapEle) {
    //   this.map = new GoogleMap(mapEle);
    //   mapEle.classList.add('show-map');

    //   this.map.isAvailable().then(() => {
    //     const position = new GoogleMapsLatLng(43.074395, -89.381056);
    //     this.map.setPosition(position);
    //   });
    // }

    // ionViewDidLoad() {
    //   let mapEle = this.map.nativeElement;

    //   if (!mapEle) {
    //     console.error('Unable to initialize map, no map element with #map view reference.');
    //     return;
    //   }

    //   // Disable this switch if you'd like to only use JS maps, as the APIs
    //   // are slightly different between the two. However, this makes it easy
    //   // to use native maps while running in Cordova, and JS maps on the web.
    //   if (this.platform.is('cordova') === true) {
    //     this.initNativeMaps(mapEle);
    //   } else {
    //     this.initJSMaps(mapEle);
    //   }
    // }

  }


  initJSMaps(mapEle) {
    // GoogleMapsLoader.KEY = 'AIzaSyDv4yp6OmngPJrOc5Vp1JatlKz9dYZWkUM';
    console.log(this.coords);
    let first_report = this.traffic_reports[0];
    let latLng = {lat: this.coords.latitude, lng: this.coords.longitude};
      if (first_report){
        latLng = {lat: parseFloat(first_report.latitude), lng: parseFloat(first_report.longitude)};
      }     

      let mapOptions = {
        center: latLng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.element, mapOptions);
      let infoWindow = new google.maps.InfoWindow;
      infoWindow.setPosition(latLng);
      infoWindow.setContent('Current Location.');
      infoWindow.open(this.map);
        // create traffic report markers,
            // label: traffic_report.user_location+": "+traffic_report.cause_of_traffic
        for (let traffic_report of this.traffic_reports){
          let position = {lat: parseFloat(traffic_report.latitude), lng: parseFloat(traffic_report.longitude)};
          let marker = new google.maps.Marker({
            position: position,
            map: this.map
          });  
          let infoWindow = new google.maps.InfoWindow;
          infoWindow.setPosition(position);
          let content = "<div style='width:220px;'><img height='200' width='200' src='"+this.api.get_image(traffic_report.picture_url)+"'/> <br/> <em>"+traffic_report.user_location+": "+traffic_report.cause_of_traffic+"</em></div>"
          infoWindow.setContent(content);
          infoWindow.open(this.map);            
        }
      let trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(this.map);              
  }

  /*initNativeMaps(mapEle) { , MarkerCluster, HtmlInfoWindow 
   // create LatLng object
      let ionic: LatLng = new LatLng(this.coords.latitude, this.coords.longitude);
      let first_report = this.traffic_reports[0];
      if (first_report){
        ionic = new LatLng(parseFloat(first_report.latitude), parseFloat(first_report.longitude));
      }
      // listen to MAP_READY event
      // You must wait for this event to fire before adding something to the map or modifying it in anyway
      this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        console.log('Map is ready!')
      // create CameraPosition
      let position: CameraPosition = {
        target: ionic,
        zoom: 14
      };

      // move the map's camera to position
      this.map.moveCamera(position);
      this.map.setTrafficEnabled(true);        

        // create traffic report markers
        for (let traffic_report of this.traffic_reports){
          let position: LatLng = new LatLng(parseFloat(traffic_report.latitude), parseFloat(traffic_report.longitude));
          let markerOptions: MarkerOptions = {
            position: position,
            title: traffic_report.user_location
          };
          let htmlInfoWindow = new HtmlInfoWindow();
            htmlInfoWindow.setContent("<div style='width:220px;'><img height='200' width='200' src='"+this.api.get_image(traffic_report.picture_url)+"'/> <br/> <em>"+traffic_report.user_location+": "+traffic_report.cause_of_traffic+"</em></div>");
           let marker = this.map.addMarker(markerOptions)
           .then((marker: Marker) => {
              // marker.showInfoWindow();
              htmlInfoWindow.open(marker);
            });     
        }
//ionic cordova plugin add https://github.com/mapsplugin/cordova-plugin-googlemaps#multiple_maps --variable API_KEY_FOR_ANDROID="AIzaSyALuVpRnt-3MC50pCs-G-m9I6LIs0V70FE" --variable API_KEY_FOR_IOS="AIzaSyCEdPZNs8kHmTUKuvueE0GyBWUxX4kJg2A"
      });
    }*/

}
