import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedsPage } from './feeds';
import { SharedModule } from './../../app/shared/shared.module';
import { MomentModule } from 'angular2-moment';
import { PipesModule } from './../../pipes/pipes.module';
// import { AutoCompleteModule } from 'ionic2-auto-complete';
import { DirectivesModule } from './../../directives/directives.module';
import { Ng2CompleterModule } from "ng2-completer";
import { MentionModule } from 'angular2-mentions/mention';


@NgModule({
  declarations: [
    FeedsPage,    
  ],
  imports: [
    IonicPageModule.forChild(FeedsPage),
    SharedModule,
    MomentModule,
    PipesModule,
    DirectivesModule,    
    Ng2CompleterModule,
    MentionModule
  ],
  exports: [
    FeedsPage
  ]
})
export class FeedsPageModule {}