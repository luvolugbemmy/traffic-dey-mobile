import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedCreatePage } from './feed-create';
import { SharedModule } from './../../app/shared/shared.module';

@NgModule({
  declarations: [
    FeedCreatePage,    
  ],
  imports: [
    IonicPageModule.forChild(FeedCreatePage),
    SharedModule    
  ],
  exports: [
    FeedCreatePage
  ]
})
export class FeedCreatePageModule {}