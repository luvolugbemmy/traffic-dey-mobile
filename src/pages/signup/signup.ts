import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, AlertController, ActionSheetController, LoadingController, Loading } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';

import { MainPage } from '../../pages/pages';
import { User } from '../../providers/user';
import { Api } from '../../providers/api';

import { TranslateService } from '@ngx-translate/core';
import base64 from 'base-64';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { first_name: string, last_name: string, username: string, email: string, password: string, password_confirmation: string, image: string } = {
    first_name: '',
    last_name: '',
    username: '',
    email: '',
    password: '',
    password_confirmation: '',
    image: null
  };
  pix_class;pix_url;camera_options;loading: Loading;show_camera;

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public api: Api,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public camera: Camera,
    public imagePicker: ImagePicker,
    public actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController, 
    private loadingCtrl: LoadingController) {
    this.pix_class = "camera-icon";
    this.pix_url = "";
    this.show_camera = true;
          // this.pix_class = "splash-logo";
          // this.pix_url = "assets/img/appicon.png";     
    this.camera_options = {
        quality : 85, 
        destinationType : this.camera.DestinationType.DATA_URL, 
        sourceType : this.camera.PictureSourceType.CAMERA, 
        correctOrientation: true,
        allowEdit : true,
        encodingType: this.camera.EncodingType.PNG,
        targetWidth: 600,
        targetHeight: 600,
        // popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false      
      }    
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  show_new_pix(){
    if(this.account.image){
      this.pix_class = "splash-logo";
      this.pix_url = this.account.image;
      this.show_camera = false;
    }    
  }


  doSignup() {
    // Attempt to login in through our User service
    this.account.password = this.account.password ? base64.encode(this.account.password) : null;
    this.account.password_confirmation = this.account.password_confirmation ? base64.encode(this.account.password_confirmation) : null;
    this.showLoading();
    this.api.post('users', this.account).subscribe((resp) => {
      this.loading.dismiss();
      setTimeout(() => {
        let response = JSON.parse(resp["_body"]);
        console.log(response);
        if (response.code === 200){
          this.signupErrorString = "Welcome to Traffic-Dey dear @"+response.data.current_user.username;
          this.api.setUserSession(response.data.authentication_token, response.data.current_user);
          this.navCtrl.setRoot("FeedsPage");  
        } else {
          this.account.password = "";
          this.account.password_confirmation = "";
          this.signupErrorString = response.errors.join(', ')
        }
        this.show_toast(this.signupErrorString);
      })
      
    }, (err) => {
      this.loading.dismiss();
      this.account.password = "";
      this.account.password_confirmation = "";      
      let error = JSON.parse(err["_body"]);
      // this.navCtrl.push(MainPage); // TODO: Remove this when you add your signup endpoint
      // this.show_toast(this.signupErrorString);
      // Unable to sign up

    });  
  }

take_picture = function(){
    this.camera.getPicture(this.camera_options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64:
     this.account.image = 'data:image/png;base64,' + imageData
     this.show_new_pix();
    }, (err) => {
     // Handle error
    });
  }


  encodeImageUri(url, callback){
    let img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = (evt) => {
      // console.log(img.height)
      // let target = evt.target || evt.srcElement;
      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');        
      canvas.height = img.height;
      canvas.width = img.width;
      ctx.drawImage(img, 0, 0);
      let dataURL = canvas.toDataURL("image/png");
      callback(dataURL);
      canvas = null; 
    };
    img.src = url;   
  }  

  choose_picture() {
    let options = {
     maximumImagesCount: 1,
     width: 600,
     height: 600,
     quality: 85
    };    
    this.imagePicker.getPictures(options).then(
      (results) => {
      for (let uri of results) {
        console.log('Image URI: ' + uri);
        this.encodeImageUri(""+uri, (base64Img) => {
          this.account.image = ""+base64Img;
          this.show_new_pix();;       
        });            
      }
      // console.log(this.images);
    }, (err) => {
       this.show_toast("Error Occured: "+err)
     }
    );
  }

showPhotoActionSheet() {
     let actionSheet = this.actionSheetCtrl.create({
       title: 'Update Picture',
       buttons: [
         {
           text: 'Delete',
           role: 'destructive',
           handler: () => {
             this.account.image = null;
             this.show_camera = true;
             console.log('Destructive clicked');
           }
         },
         {
           text: 'Take Picture',
           handler: () => {
           this.take_picture();         
           }
         },
         {
           text: 'Choose from Album',
           handler: () => {
             this.choose_picture();
           }
         },
         {
           text: 'Cancel',
           role: 'cancel',
           handler: () => {
             console.log('Cancel clicked');
           }
         }
       ]
     });

     actionSheet.present();
   }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  showError(title, text) {
    setTimeout(() => {
      // this.loading.dismiss();
    });
 
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
      

  show_toast(message){
          let toast = this.toastCtrl.create({
        message: message,
        duration: 8000,
        position: 'bottom',
        showCloseButton: true
      });
      toast.present();  
  }

  ionViewDidLoad() {
    // Image Test
    // setTimeout(() => {
    //   this.pix_class = "splash-logo";
    //   this.pix_url = "assets/img/appicon.png";
    //   this.show_camera = false;
    // }, 5000)  
    console.log('ionViewDidLoad Sign Up Page');
  }  
}
