import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

import { MainPage } from '../../pages/pages';

import { User } from '../../providers/user';
import { Api } from '../../providers/api';

import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: '',
    password: ''
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public api: Api,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  // Attempt to login in through our User service
  doLoginIt() {
    this.user.login(this.account).subscribe((resp) => {
    let response = JSON.parse(resp["_body"]);
        console.log(response);
        if (response.code === 200){
          this.loginErrorString = "Success!"
          this.api.setUserSession(response.data.authentication_token, response.data.current_user);
          this.navCtrl.push(MainPage);  
        } else {
          this.loginErrorString = response.errors.join(', ')
        }
        this.show_toast(this.loginErrorString);      
    }, (err) => {
      // this.navCtrl.push(MainPage);
      // Unable to log in
        let error = JSON.parse(err["_body"]);
        this.show_toast(this.loginErrorString);
    });
  }

  show_toast(message){
          let toast = this.toastCtrl.create({
        message: message,
        duration: 8000,
        position: 'bottom',
        showCloseButton: true
      });
      toast.present();  
  }  
}
