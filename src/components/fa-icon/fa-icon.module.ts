import { NgModule } from '@angular/core';
import { IonicPageModule } from "ionic-angular";
import { FaIcon} from './fa-icon';

@NgModule({
	declarations: [FaIcon],
	imports: [IonicPageModule.forChild(FaIcon)],
	exports: [FaIcon]
})
export class FaIconModule {}
