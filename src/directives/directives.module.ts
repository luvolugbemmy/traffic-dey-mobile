import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PressDirective } from './press/press';
import { DbltapDirective } from './dbltap/dbltap';
// import { MentionDirective } from './mention/mention';
// import { MentionListComponent } from './mention/mention-list.component';
@NgModule({
	declarations: [
	PressDirective,
    DbltapDirective,
    ],
	imports: [],
	exports: [
		PressDirective,
    	DbltapDirective,
    ]
})
export class DirectivesModule {}

// static forRoot(): ModuleWithProviders {
//         return {
//             ngModule: DirectivesModule
//         };
//     }