import { Directive, ElementRef, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Gesture } from 'ionic-angular/gestures/gesture';

declare var Hammer;


/**
 * Generated class for the LongpressDirective directive.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
 * for more info on Angular Directives.
 */
@Directive({
  selector: '[longpress]' // Attribute selector
})
export class PressDirective implements OnInit, OnDestroy {
	el: HTMLElement;
  	pressGesture: Gesture;

@Output() longpress = new EventEmitter();

  constructor(el: ElementRef) {
    this.el = el.nativeElement;
  }

	ngOnInit() {
      this.pressGesture = new Gesture(this.el, {
        recognizers: [
          [Hammer.Press, {time: 6000}]
        ]
      });
	    this.pressGesture.listen();
	    this.pressGesture.on('press', e => {
	      this.longpress.emit(e);
	    })
  	}

  ngOnDestroy() {
    this.pressGesture.destroy();
  }
}
