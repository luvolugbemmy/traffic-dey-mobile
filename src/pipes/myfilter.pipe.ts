import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'myfilter',
    pure: false
})
// @Injectable()
export class MyFilterPipe implements PipeTransform {
  transform(items: any[], keyword: string): any {
  	// filter items array, items which match and return true will be kept, false will be filtered out
  	let filtered = ([null,undefined,""].indexOf(keyword) >= 0) ? items : [];		
  	items.forEach((item) => {
  	Object.keys(item).map((key) => {
  		// keys.push(key);
  		let actual =  "";
  		(''+item[key]).split(' ').forEach((word) => {
  				actual+=''+word.toLowerCase();
  		
  			let expected = ([null,undefined,""].indexOf(keyword) >= 0) ? '' : ''+keyword.toLowerCase();
    		if (actual && (actual.length) > 0 && keyword && ( actual.indexOf(expected) >= 0 ) ){
    			if(filtered.indexOf(item) <= -1)
					filtered.push(item);
				}
			});
   	});
   });
   return filtered;        	        	
  }
}