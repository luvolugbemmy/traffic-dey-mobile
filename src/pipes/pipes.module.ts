import { NgModule } from '@angular/core';
import { CapitalizePipe } from './capitalize.pipe';
import { MyFilterPipe } from './myfilter.pipe';

 
@NgModule({
    declarations: [
        CapitalizePipe,
        MyFilterPipe,
    ],
    imports: [
 
    ],
    exports: [
        CapitalizePipe,
        MyFilterPipe,
    ]
})
export class PipesModule {}