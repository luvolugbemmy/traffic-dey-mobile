import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Config, ToastController, AlertController, ActionSheetController, ModalController, LoadingController, Loading } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import { SplashScreen } from '@ionic-native/splash-screen';

import { CardsPage } from '../pages/cards/cards';
import { ContentPage } from '../pages/content/content';
import { FirstRunPage } from '../pages/pages';
// import { ListMasterPage } from '../pages/list-master/list-master';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { MenuPage } from '../pages/menu/menu';
import { SearchPage } from '../pages/search/search';
import { SettingsPage } from '../pages/settings/settings';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
// import { WelcomePage } from '../pages/welcome/welcome';

import { Settings } from '../providers/providers';
import { Api } from '../providers/api';

import { TranslateService } from '@ngx-translate/core'

declare var navigator: any;
declare var Connection: any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage = "WelcomePage";

  @ViewChild(Nav) nav: Nav;menu_background;user_avatar;user_session;loading: Loading;

  pages: any[] = [
    { title: 'Traffic Map', component: "MapPage", icon: 'map'},
    { title: 'Feeds', component: "SearchPage", icon: 'logo-rss'},
    { title: 'Route Feeds', component: "ListMasterPage", icon: 'navigate'},
    { title: 'Followers', component: "ListMasterPage"  , icon: 'people'},
    { title: 'My Points', component: "MenuPage", icon: 'thumbs-up'}    
  ]

// { title: 'Tutorial', component: TutorialPage, icon: 'none'},
//     { title: 'Welcome', component: WelcomePage  , icon: 'none'},
//     { title: 'Tabs', component: TabsPage  , icon: 'none'},
//     { title: 'Cards', component: CardsPage  , icon: 'none'},
//     { title: 'Content', component: ContentPage  , icon: 'none'},
//     { title: 'Login', component: LoginPage  , icon: 'none'},
//     { title: 'Signup', component: SignupPage  , icon: 'none'},
//     { title: 'Map', component: MapPage  , icon: 'none'},
//     { title: 'Master Detail', component: ListMasterPage  , icon: 'none'},
//     { title: 'Menu', component: MenuPage  , icon: 'none'},
//     { title: 'Settings', component: SettingsPage  , icon: 'none'},
//     { title: 'Search', component: SearchPage , icon: 'none' }
  constructor(private translate: TranslateService, private api: Api,
  private toastCtrl: ToastController, 
    private alertCtrl: AlertController,
  public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController, private loadingCtrl: LoadingController,
    private platform: Platform, settings: Settings, private config: Config, 
    private statusBar: StatusBar, private splashScreen: SplashScreen,
    public keyboard: Keyboard) {
    this.menu_background = "assets/img/sidemenu-background.png";
    this.user_session = this.api.getUserSession();
    this.api.userSessionChange.subscribe((data) => {
      console.log(data)
      this.user_session = data.user_session;
      // this.user_avatar = (this.user_session && ([null,undefined,""].indexOf(this.user_session.get_pix_url) <= -1) && this.api.isValidURL(this.user_session.get_pix_url))  ? this.user_session.get_pix_url : "assets/images/mab.png";    
    });    
    // this.initTranslate();        
    this.initializeApp();
  }

  ionViewDidLoad() {

  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  checkNetwork() {
    if(navigator.connection && (navigator.connection.type == "No network connection")) {
      this.showToast("Please check your internet connection or try again later.", 10000, "bottom")  
    }
  }  

  showToast(message, duration, position) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  initializeApp(){
    if(this.user_session && this.api.getAuthToken()){      
      this.rootPage = "FeedsPage";
    }      
   // this.api.checkNetwork.subscribe((value) => {
   //         console.log("Check Network Connection event fired successfully");
   //         this.checkNetwork();
   //      });    
      this.platform.ready().then(() => {
        this.checkNetwork();
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.statusBar.overlaysWebView(false);        
        this.statusBar.styleDefault();
        this.keyboard.hideKeyboardAccessoryBar(false);
        this.splashScreen.hide();
      });
    this.api.getTrafficReportSetup();
    this.api.reload_user_settings();    
  }

  logout() {
    let confirm = this.alertCtrl.create({
      title: "Logout",
      message: "Are you sure?",
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Ok',
          handler: () => {
          this.showLoading();     
            this.api.logout().subscribe(succ => {
              this.loading.dismiss();
                this.nav.setRoot("WelcomePage")
            });
          }
         }
       ]
    });
    confirm.present();
   }             
  // }  
  openPage(component) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(component);
  }
}
