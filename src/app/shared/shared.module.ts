import { NgModule } from '@angular/core';
import { IonicPageModule } from "ionic-angular";
import { FaIcon} from './../../components/fa-icon/fa-icon';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpModule, Http } from '@angular/http';
import { PipesModule } from './../../pipes/pipes.module';
import { MomentModule } from 'angular2-moment';

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './../../assets/i18n/', '.json');
}

@NgModule({
	declarations: [FaIcon],
	imports: [IonicPageModule.forChild(FaIcon),
	 TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
   PipesModule,
   MomentModule
	],
	exports: [FaIcon]
})
export class SharedModule {}
