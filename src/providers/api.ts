import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Http, RequestOptions, URLSearchParams, Headers } from '@angular/http';
import {Observable, Observer} from 'rxjs';
import 'rxjs/add/operator/map';
import { StorageProvider } from './storage/storage';

/**
 * Api is a generic REST Api handler. Set your API url first. traffic-dey.herokuapp.com
 */
@Injectable()
export class Api {
  url: string = 'https://traffic-dey.herokuapp.com/api/v1';req_options;goCheckNetwork;checkNetwork;userSession;
   traffic_causes;traffic_ratings;
  userSessionChange: Observable<any>;userSessionChangeObserver: Observer<any>;
  constructor(public http: Http, public storage: StorageProvider, private platform: Platform) {
    this.traffic_causes = [];
    this.traffic_ratings = [];     
    this.checkNetwork = new Observable((observer) => {
        this.goCheckNetwork = observer;
    });    
    this.reload_headers();
    this.userSessionChange = new Observable((observer) => {
      this.userSessionChangeObserver = observer;      
    });     
  }

  public reload_headers(){  
    let headers = new Headers();
    headers.append('X-Parse-REST-API-Key', 'TrP7kc0tb4ECTQeG1XQMmSGk80xGl4dB');
    headers.append('X-USER-TOKEN', this.getAuthToken());
    headers.append('X-USER-EMAIL', this.getUserEmail());
    headers.append('Content-Type', 'application/json'); //'application/json; charset=utf-8');//
    this.req_options = new RequestOptions({ headers: headers });  
    this.goCheckNetwork && this.goCheckNetwork.next(true);
  }

  public getUserSession() {
    // if (this.platform.is('cordova') === true) {
    //   return this.storage.get_item("_traffic_dey_user_session");
    // }else {
      return JSON.parse(window.localStorage.getItem("_traffic_dey_user_session"));
    // }
  }

  public logout() {
    this.reload_headers();
    return Observable.create(observer => {
      // this.currentUser = null;
      this.delete("users/logout")
      .subscribe(
      data => {
        setTimeout(() => {
          let response = JSON.parse(data["_body"])
          if(response.message == "SUCCESS") {
            // if (this.platform.is('cordova') === true) {
            //   this.storage.remove_item("_traffic_dey_authentication_token");
            //   this.storage.remove_item("_traffic_dey_user_session");             
            // } else {
              localStorage.removeItem("_traffic_dey_authentication_token");
              localStorage.removeItem("_traffic_dey_user_session");
            // }
            this.userSession = null;
            observer.next(true);
            observer.complete();
          }else{
            observer.next(false);
          }
        });
      });
    })
  }

  public getAuthToken() {
    // if (this.platform.is('cordova') === true) {
    //   return ""+this.storage.get_item("_traffic_dey_authentication_token");
    // } else {
      return window.localStorage.getItem("_traffic_dey_authentication_token");
    // }
  } 

  public getUserEmail() {
    if (this.getUserSession()){
      return this.getUserSession().email;
    }else{
      return "";
    }
  }

  setUserSession(authentication_token: any, user_session: any) {
    this.userSession = {authentication_token: authentication_token, user_session: user_session};
    // if (this.platform.is('cordova') === true) {
    //     this.storage.remove_item("_traffic_dey_authentication_token");
    //     this.storage.remove_item("_traffic_dey_user_session");
    //     this.storage.set_item("_traffic_dey_authentication_token", authentication_token);
    //     this.storage.set_item("_traffic_dey_user_session", user_session);         
    // } else {
      localStorage.removeItem("_traffic_dey_authentication_token");
      localStorage.removeItem("_traffic_dey_user_session");      
      localStorage.setItem("_traffic_dey_authentication_token", authentication_token);
      localStorage.setItem("_traffic_dey_user_session", JSON.stringify(user_session));    
    // }    
    this.userSessionChangeObserver.next(this.userSession);
    this.reload_headers();
  }


  store_users_settings(settings) {
      localStorage.removeItem("_traffic_dey_settings");
      localStorage.setItem("_traffic_dey_settings", JSON.stringify(settings)); 
  }

   public get_users_settings() {
      return JSON.parse(window.localStorage.getItem("_traffic_dey_settings"));
  }


  public reload_user_settings(){
    let settings = this.get_users_settings();
    if (settings) {
      // code...
    } else {
      this.store_users_settings({feeds_view: "list", geolocation: {enableHighAccuracy: false, timeout: 10000, maximumAge: 0}, distance: 13})
    }    
  }

  getTrafficReportSetup(){
  this.get("traffic_reports/get_traffic_report_form_setups/")
    .subscribe((resp) => {
      // this.loading.dismiss();
      let response = JSON.parse(resp["_body"]);
        console.log(response);
        if (response.code === 200){
          this.traffic_causes = response.traffic_causes;
          this.traffic_ratings = response.traffic_ratings;          
        }             
    }, (err) => {      
      // this.loading.dismiss();
    });        
  }  

  isValidURL(url){
    var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

    if(RegExp.test(url)){
        return true;
    }else{
        return false;
    }
  } 

  public is_browser(){
    if (this.platform.is('cordova')) {
      return false;
    }else{
      return true;
   }
  }


  public get_image(url){
    return (url && ([null,undefined,""].indexOf(url) <= -1) && this.isValidURL(url))  ? url : "assets/img/no_icon.png";
  }

  public get_avatar(url){
    return (url && ([null,undefined,""].indexOf(url) <= -1) && this.isValidURL(url))  ? url : "assets/img/avatar.png";
  }  
      
  get(endpoint: string, params?: any, options?: RequestOptions) {
    if (!options) {
      options = new RequestOptions();
    }

    // Support easy query params for GET requests
    if (params) {
      let p = new URLSearchParams();
      for (let k in params) {
        p.set(k, params[k]);
      }
      // Set the search field if we have params and don't already have
      // a search field set in options.
      options.search = !options.search && p || options.search;
    }
    // this.reload_headers();
    return this.http.get(this.url + '/' + endpoint, this.req_options);
  }

  post(endpoint: string, body: any, options?: RequestOptions) {
    // this.reload_headers();
    return this.http.post(this.url + '/' + endpoint, body, this.req_options);
  }

  put(endpoint: string, body: any, options?: RequestOptions) {
    // this.reload_headers();
    return this.http.put(this.url + '/' + endpoint, body, this.req_options);
  }

  delete(endpoint: string, options?: RequestOptions) {
    // this.reload_headers();
    return this.http.delete(this.url + '/' + endpoint, this.req_options);
  }

  patch(endpoint: string, body: any, options?: RequestOptions) {
    // this.reload_headers();
    return this.http.put(this.url + '/' + endpoint, body, this.req_options);
  }
}
