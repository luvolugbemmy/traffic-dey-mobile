import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { NativeStorage } from '@ionic-native/native-storage';
/*
  Generated class for the StorageProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class StorageProvider {

  constructor(public http: Http, private nativeStorage: NativeStorage, private platform: Platform) {
    console.log('Hello StorageProvider Provider');
  }


	set_item(key, data){
		return this.nativeStorage.setItem(key, data)
		  // .then(
		  //   () => console.log('Stored item!'),
		  //   error => console.error('Error storing item', error)
		  // );
	}

	get_item(key){
		let item = {}
		this.nativeStorage.getItem(key)
	  .then(
	    data => {
	    	console.log(data)
	    	item = data
	    },
	    error => console.error(error)
	  ); 
	  return item; 
	}

	remove_item(key){
		return this.nativeStorage.remove(key)
	  // .then(
	  //   data => console.log(data),
	  //   error => console.error(error)
	  // );  
	}	

}
