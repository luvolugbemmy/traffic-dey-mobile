import {AutoCompleteService} from 'ionic2-auto-complete';
import { Http } from '@angular/http';
import { Api } from '../providers/api'; 
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map'

@Injectable()
export class AutoCompleteApi implements AutoCompleteService {
  labelAttribute = "username";

  constructor(private api:Api) {

  }
  getResults(keyword:string) {
    return this.api.get("users/search_by_username/"+keyword)
      .map(
        result =>
        {
          return result.json()
            .filter(item => item.username.toLowerCase().startsWith(keyword.toLowerCase()) )
        });
  }
}